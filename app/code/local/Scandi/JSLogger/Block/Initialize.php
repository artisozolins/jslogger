<?php
/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_JSLogger_Block_Initialize extends Mage_Core_Block_Template
{
    /**
     * @return string
     */
    protected function _toHtml()
    {
        if (!$this->helper('jslogger')->isEnabled()) {
            return '';
        }
        return parent::_toHtml();
    }
}