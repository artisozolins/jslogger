<?php
/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_JSLogger_Block_Adminhtml_Log_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    /**
     * @return Scandi_JSLogger_Block_Adminhtml_Log_Edit_Form
     */
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post',
            'enctype' => 'multipart/form-data'
        ));

        $log = Mage::registry('current_log');

        $fieldset = $form->addFieldset('info',
            array(
                'legend' => Mage::helper('jslogger')->__('Log Details'),
                'class' => 'fieldset-wide'
            )
        );

        $fieldset->addField('log_id', 'label',
            array(
                'label' => $this->__('Log id'),
                'value' => $log->getId()
            )
        );

        $fieldset->addField('message', 'label',
            array(
                'label' => $this->__('Message'),
                'value' => $log->getMessage()
            )
        );

        $fieldset->addField('time', 'label',
            array(
                'label' => $this->__('Time'),
                'value' => $log->getTime()
            )
        );

        $fieldset->addField('url', 'label',
            array(
                'label' => $this->__('Url'),
                'value' => $log->getUrl()
            )
        );

        $fieldset->addType('wrapped_label', 'Scandi_JSLogger_Model_Form_Element_Label_Wrapped');

        $fieldset->addField('stack_trace', 'wrapped_label',
            array(
                'label' => $this->__('Stack trace'),
                'value' => $log->getStackTrace(),
                'html'  => 1,
                'wrapper_html' => '<pre>%s</pre>'
            )
        );

        $fieldset->addField('viewport_width', 'label',
            array(
                'label' => $this->__('Viewport width'),
                'value' => $log->getViewportWidth()
            )
        );

        $fieldset->addField('viewport_height', 'label',
            array(
                'label' => $this->__('Viewport height'),
                'value' => $log->getViewportHeight()
            )
        );

        $fieldset->addField('user_agent', 'label',
            array(
                'label' => $this->__('User agent string'),
                'value' => $log->getUserAgent()
            )
        );

        $store = Mage::app()->getStore($log->getStoreId());
        $website = $store->getWebsite();

        $fieldset->addField('store', 'label',
            array(
                'label' => $this->__('Store information'),
                'value' => $this->__(
                        'Website \'%s\' (id: %s, code: %s), store \'%s\' (id: %s, code: %s)',
                        $website->getName(),
                        $website->getId(),
                        $website->getCode(),
                        $store->getName(),
                        $store->getId(),
                        $store->getCode()
                    )
            )
        );

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}