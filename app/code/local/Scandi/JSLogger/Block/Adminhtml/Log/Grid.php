<?php
/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_JSLogger_Block_Adminhtml_Log_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        if (Mage::helper('jslogger')->isEnabled()) {
            $text = 'Logging is currently enabled in default configuration scope, setting may vary in other scopes';
        } else {
            $text = 'Logging is currently disabled in default configuration scope, setting may vary in other scopes';
        }
        Mage::getSingleton("adminhtml/session")->addNotice(Mage::helper("jslogger")->__($text));

        parent::__construct();
        $this->setId('logGrid');
        $this->setDefaultSort('log_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    /**
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    protected function _prepareCollection()
    {
        $collection = Mage::getModel('jslogger/log')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    /**
     * @return Scandi_JSLogger_Block_Adminhtml_Log_Grid
     */
    protected function _prepareColumns()
    {
        $this->addColumn('log_id', array(
            'header' => Mage::helper('jslogger')->__('ID'),
            'align' =>'right',
            'width' => '50px',
            'type' => 'number',
            'index' => 'log_id',
        ));

        $this->addColumn('message', array(
            'header' => Mage::helper('jslogger')->__('Message'),
            'index' => 'message',
        ));

        $this->addColumn('url', array(
            'header' => Mage::helper('jslogger')->__('Url'),
            'index' => 'url',
        ));

        $this->addColumn('stack_trace', array(
            'header' => Mage::helper('jslogger')->__('Stack Trace'),
            'index' => 'stack_trace',
        ));

        $this->addColumn('time', array(
            'header' => Mage::helper('jslogger')->__('Time'),
            'index' => 'time',
            'type'      => 'datetime',
            'gmtoffset' => true
        ));

        return parent::_prepareColumns();
    }

    /**
     * @param  array $row
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/adminhtml_log/view', array('id' => $row['log_id']));
    }

    /**
     * @return Scandi_JSLogger_Block_Adminhtml_Log_Grid
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('log_id');
        $this->getMassactionBlock()->setFormFieldName('log_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_log', array(
            'label'=> Mage::helper('jslogger')->__('Remove Log'),
            'url'  => $this->getUrl('*/adminhtml_log/massRemove'),
            'confirm' => Mage::helper('jslogger')->__('Are you sure?')
        ));
        return $this;
    }
}