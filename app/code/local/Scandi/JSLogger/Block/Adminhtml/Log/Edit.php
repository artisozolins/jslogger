<?php
/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_JSLogger_Block_Adminhtml_Log_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'jslogger';
        $this->_mode = 'edit';
        $this->_controller = 'adminhtml_log';

        parent::__construct();

        $this->_updateButton('delete', 'label', Mage::helper('jslogger')->__('Delete Log'));
        $this->_removeButton('save');
        $this->_removeButton('reset');
    }

    /**
     * @return string
     */
    public function getHeaderText()
    {
        return Mage::helper('jslogger')->__('Log Details');
    }
}