<?php
/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_JSLogger_Block_Adminhtml_Log extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = "adminhtml_log";
        $this->_blockGroup = "jslogger";
        $this->_headerText = Mage::helper("jslogger")->__("Log Manager");
        $this->_addButtonLabel = Mage::helper("jslogger")->__("Add New Item");
        parent::__construct();
        $this->_removeButton('add');
    }
}