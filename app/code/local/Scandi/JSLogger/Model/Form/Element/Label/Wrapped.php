<?php
/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_JSLogger_Model_Form_Element_Label_Wrapped extends Varien_Data_Form_Element_Label
{
    /**
     * @param array $attributes
     */
    public function __construct($attributes = array())
    {
        $this->setData('wrapper_html', $attributes['wrapper_html']);
        parent::__construct($attributes);
    }

    /**
     * @return string
     */
    public function getElementHtml()
    {
        return sprintf($this->getData('wrapper_html'), parent::getElementHtml());
    }
}