<?php
/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_JSLogger_Model_Mysql4_Log extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init("jslogger/log", "log_id");
    }

    public function cleanOldEntries()
    {
        $sub = $this->_getReadAdapter()->select()
            ->from($this->getMainTable())
            ->limit($this->getMaxEntryCount())
            ->order('log_id DESC');

        $maxSelect = $this->_getReadAdapter()->select()
            ->from($sub, array('max_id' => 'MIN(log_id)'));

        /* Max id for log to be deleted */
        $maxId = $maxSelect->query()->fetchColumn();
        $this->_getWriteAdapter()->delete($this->getMainTable(), sprintf('log_id < %s', $maxId));
    }

    /**
     * Max entries allowed in database
     *
     * @return int
     */
    public function getMaxEntryCount()
    {
        $config = (int) Mage::getStoreConfig('jslogger/general/max_entries');
        if ($config) {
            return $config;
        }
        return 2000;
    }
}