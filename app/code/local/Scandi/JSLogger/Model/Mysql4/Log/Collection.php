<?php
/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_JSLogger_Model_Mysql4_Log_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        $this->_init("jslogger/log");
    }
}
