<?php
/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 *
 * @method int getLogId()
 * @method Scandi_JSLogger_Model_Log setLogId(int $logId)
 * @method string getMessage()
 * @method Scandi_JSLogger_Model_Log setMessage(string $message)
 * @method string getStackTrace()
 * @method Scandi_JSLogger_Model_Log setStackTrace(string $traceText)
 * @method int getViewportWidth()
 * @method Scandi_JSLogger_Model_Log setViewportWidth(string $width)
 * @method int getViewportHeight()
 * @method Scandi_JSLogger_Model_Log setViewportHeight(int $height)
 * @method string getUserAgent()
 * @method Scandi_JSLogger_Model_Log setUserAgent(int $agentString)
 * @method string getUrl()
 * @method Scandi_JSLogger_Model_Log setUrl(string $url)
 * @method string getTime()
 * @method Scandi_JSLogger_Model_Log setTime(string $timestamp)
 * @method int getStoreId()
 * @method Scandi_JSLogger_Model_Log setStoreId(int $logId)
 */
class Scandi_JSLogger_Model_Log extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("jslogger/log");
    }
}