<?php
/* @var $this Mage_Core_Model_Resource_Setup */

$table = $this->getConnection()
    ->newTable($this->getTable('jslogger/log'))
    ->addColumn('log_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ))
    ->addColumn('message', Varien_Db_Ddl_Table::TYPE_TEXT)
    ->addColumn('stack_trace', Varien_Db_Ddl_Table::TYPE_TEXT)
    ->addColumn('viewport_width', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
    ))
    ->addColumn('viewport_height', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
    ))
    ->addColumn('user_agent', Varien_Db_Ddl_Table::TYPE_TEXT)
    ->addColumn('url', Varien_Db_Ddl_Table::TYPE_VARCHAR, 2048)
    ->addColumn('time', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable' => false,
        'default'  => 'CURRENT_TIMESTAMP'
    ))
    ->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
        'unsigned'  => true,
    ));

$this->getConnection()->createTable($table);