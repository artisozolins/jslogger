<?php
/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_JSLogger_Adminhtml_LogController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout()->_setActiveMenu("jslogger/log")->_addBreadcrumb(
            Mage::helper("adminhtml")->__("Log  Manager"),
            Mage::helper("adminhtml")->__("Log Manager")
        );
        return $this;
    }

    public function indexAction()
    {
        $this->_title($this->__("JSLogger"));
        $this->_title($this->__("Manager Log"));

        $this->_initAction();
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__("JSLogger"));
        $this->_title($this->__("Log"));
        $this->_title($this->__("Edit Item"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("jslogger/log")->load($id);
        if ($model->getId()) {
            Mage::register("log_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("jslogger/log");
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent(
                $this->getLayout()->createBlock("jslogger/adminhtml_log_edit")
            )->_addLeft($this->getLayout()->createBlock("jslogger/adminhtml_log_edit_tabs"));
            $this->renderLayout();
        }
        else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("jslogger")->__("Item does not exist."));
            $this->_redirect("*/*/");
        }
    }

    public function viewAction()
    {
        $logId = $this->getRequest()->getParam('id');
        $log = Mage::getModel('jslogger/log')->load($logId);

        if (!$log->getId()) {
            Mage::getSingleton("adminhtml/session")->addError(
                Mage::helper("jslogger")->__('Log with id %s doesn\'t exist.', $logId)
            );
            $this->_redirect('*/*/index');
            return;
        }

        Mage::register('current_log', $log);
        $this->loadLayout();
        $this->renderLayout();
    }

    public function newAction()
    {

        $this->_title($this->__("JSLogger"));
        $this->_title($this->__("Log"));
        $this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
        $model  = Mage::getModel("jslogger/log")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("log_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("jslogger/log");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);


        $this->_addContent($this->getLayout()->createBlock("jslogger/adminhtml_log_edit"))
            ->_addLeft($this->getLayout()->createBlock("jslogger/adminhtml_log_edit_tabs"));

        $this->renderLayout();

    }

    public function saveAction()
    {
        $post_data=$this->getRequest()->getPost();
        if ($post_data) {

            try {
                $model = Mage::getModel("jslogger/log")
                    ->addData($post_data)
                    ->setId($this->getRequest()->getParam("id"))
                    ->save();

                Mage::getSingleton("adminhtml/session")->addSuccess(
                    Mage::helper("adminhtml")->__("Log was successfully saved")
                );
                Mage::getSingleton("adminhtml/session")->setLogData(false);

                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            }
            catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setLogData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }

        }
        $this->_redirect("*/*/");
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam("id") > 0 ) {
            try {
                $model = Mage::getModel("jslogger/log");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(
                    Mage::helper("adminhtml")->__("Item was successfully deleted")
                );
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction()
    {
        try {
            $ids = $this->getRequest()->getPost('log_ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("jslogger/log");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(
                Mage::helper("adminhtml")->__("Item(s) was successfully removed")
            );
        }
        catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }
}
