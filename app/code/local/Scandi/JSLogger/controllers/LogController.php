<?php
/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */
class Scandi_JSLogger_LogController extends Mage_Core_Controller_Front_Action
{
    public function errorAction()
    {
        if (!Mage::helper('jslogger')->isEnabled()) {
            return;
        }

        $data = $this->getRequest()->getPost();
        $data['time'] = now();
        $data['store_id'] = Mage::app()->getStore()->getId();

        /* @var $logModel Scandi_JSLogger_Model_Log */
        $logModel = Mage::getModel('jslogger/log');
        $logModel->setData($data)->save();
        $logModel->getResource()->cleanOldEntries();
    }
}