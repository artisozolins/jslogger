/**
 * Scandi_JSLogger
 *
 * @category     Scandi
 * @package      Scandi_JSLogger
 * @author       Artis Ozolins <info@scandiweb.com>
 * @copyright    Copyright (c) 2014 Scandiweb, Ltd (http://scandiweb.com)
 * @license      http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 */

/* Class that logs information about global errors back to server */
var ScandiJsLogger = Class.create({
    initialize: function(config) {
        this.config = Object.extend({}, config);
        window.onerror = this.handleError.bind(this);
        this.errors = [];
    },
    handleError: function(msg, url, line, col, error) {
        try {

            if (this.errors.indexOf(this.hashString(error.stack)) != -1) {
                return;
            }
            this.errors.push(this.hashString(error.stack));

            var data = {
                message: msg,
                stack_trace: error.stack,
                url: url,
                viewport_width: document.viewport.getWidth(),
                viewport_height: document.viewport.getHeight(),
                user_agent: navigator.userAgent
            };

            new Ajax.Request(
                this.config.logUrl, {
                    method: 'post',
                    parameters: data
                }
            );
        } catch (e) {
            console.log('Never mind...');
        }
    },
    hashString: function(string) {
        var hash = 0, i, chr, len;
        if (string.length == 0) return hash;
        for (i=0, len=string.length; i<len; i++) {
            chr   = string.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0;
        }
        return hash;
    }
});